#!/bin/sh
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
GRAY='\033[0;37m'
NC='\033[0m' # No Color
printf "${BLUE}-----------------------------|                 ${RED}CODE HOP${BLUE}                 |-----------------------------${NC}\n"
printf "${GREEN}Started to create VMs. Please do not interrupt in between!${NC}\n\n"
export PATH=$PATH:"/c/Program Files/Oracle/VirtualBox"
NODE_COUNT=1
if [ "$1" == 2 ]; then
	NODE_COUNT=2
fi

printf "${GREEN}Current configuration is configured to create $NODE_COUNT Slave Node${NC}\n\n"

sed -i "s/NODE_COUNT/$NODE_COUNT/g" Vagrantfile

vagrant up

MASTER_VM_ID=$(VBoxManage list vms | grep "master" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
SLAVE_01_VM_ID=$(VBoxManage list vms | grep "slave-01" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
SLAVE_02_VM_ID=$(VBoxManage list vms | grep "slave-02" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
if [ ! -z "$MASTER_VM_ID" -a "$MASTER_VM_ID" != " " ]; then
		MASTER_VM_ON=$(VBoxManage showvminfo $MASTER_VM_ID | grep -c "running (since")
		if [[ "$MASTER_VM_ON" == 1 ]]; then
			cp ./.vagrant/machines/master/virtualbox/private_key ./master_private_key.ppk
		    printf "${BLUE}|---------------------------------------${RED}Master VM${BLUE}--------------------------------------|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Master VM Status                      ${BLUE}|${GREEN}            ON                            ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Master IP                             ${BLUE}|${GREEN}      172.20.20.110                       ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Master Username                       ${BLUE}|${GREEN}         vagrant                          ${BLUE}|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|--------------------------------------------------------------------------------------${BLUE}|${NC}\n"
			printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"			
			printf "${BLUE}|                  ${GREEN}Use ${RED}master_private_key.ppk${GREEN} file to login into Master VM             ${BLUE}|${NC}\n"
            printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"
		    printf "${BLUE}|--------------------------------------------------------------------------------------|${NC}\n\n"
		fi
fi

if [ ! -z "$SLAVE_01_VM_ID" -a "$SLAVE_01_VM_ID" != " " ]; then
		SLAVE_01_VM_ON=$(VBoxManage showvminfo $SLAVE_01_VM_ID | grep -c "running (since")
		if [[ "$SLAVE_01_VM_ON" == 1 ]]; then
			cp ./.vagrant/machines/slave-01/virtualbox/private_key ./slave_01_private_key.ppk
			printf "${GRAY}----------------------------------------------------------------------------------------${NC}\n\n"
		    printf "${BLUE}|---------------------------------------${RED}Slave-01 VM${BLUE}------------------------------------|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-01 VM Status                    ${BLUE}|${GREEN}            ON                            ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-01 IP                           ${BLUE}|${GREEN}      172.20.20.111                       ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-01 Username                     ${BLUE}|${GREEN}         vagrant                          ${BLUE}|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|--------------------------------------------------------------------------------------${BLUE}|${NC}\n"
			printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"			
			printf "${BLUE}|                 ${GREEN}Use ${RED}slave_01_private_key.ppk${GREEN} file to login into Slave-01 VM          ${BLUE}|${NC}\n"
            printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"
		    printf "${BLUE}|--------------------------------------------------------------------------------------|${NC}\n"
		fi
fi

if [ ! -z "$SLAVE_02_VM_ID" -a "$SLAVE_02_VM_ID" != " " ]; then
		SLAVE_02_VM_ON=$(VBoxManage showvminfo $SLAVE_02_VM_ID | grep -c "running (since")
		if [[ "$SLAVE_02_VM_ON" == 1 ]]; then
			cp ./.vagrant/machines/slave-02/virtualbox/private_key ./slave_02_private_key.ppk
			printf "${GRAY}----------------------------------------------------------------------------------------${NC}\n\n"
		    printf "${BLUE}|---------------------------------------${RED}Slave-02 VM${BLUE}------------------------------------|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-02 VM Status                    ${BLUE}|${GREEN}            ON                            ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-02 IP                           ${BLUE}|${GREEN}      172.20.20.112                       ${BLUE}|${NC}\n"
			printf "${BLUE}|${RED}     Slave-02 Username                     ${BLUE}|${GREEN}         vagrant                          ${BLUE}|${NC}\n"
			printf "${BLUE}|                                           |                                          ${BLUE}|${NC}\n"
			printf "${BLUE}|--------------------------------------------------------------------------------------${BLUE}|${NC}\n"
			printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"			
			printf "${BLUE}|                 ${GREEN}Use ${RED}slave_02_private_key.ppk${GREEN} file to login into Slave-02 VM          ${BLUE}|${NC}\n"
            printf "${BLUE}|                                                                                      ${BLUE}|${NC}\n"
		    printf "${BLUE}|--------------------------------------------------------------------------------------|${NC}\n"
		fi
fi

sed -i "s/(1..$NODE_COUNT)/(1..NODE_COUNT)/g" Vagrantfile

