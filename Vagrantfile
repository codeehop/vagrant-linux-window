# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

$install_docker_script = <<SCRIPT
echo "Installing dependencies ..."
sudo apt-get update
echo Installing Docker...
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo usermod -aG docker vagrant
sudo mkdir -p /home/vagrant/logs
sudo chown -R "vagrant:vagrant" /home/vagrant/logs
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    #Common setup
    config.vm.box = "ubuntu/focal64"
    config.vm.synced_folder ".", "/vagrant"
    config.vm.provision "shell",inline: $install_docker_script, privileged: true
	config.winrm.timeout = 1800 # 30 minutes
	config.vm.boot_timeout = 1800 # 30 minutes
    config.vm.provider "virtualbox" do |vb|
      vb.memory = 1024
    end

#Setup Master Nodes
    config.vm.define "master" do |master|
         master.vm.network :private_network, ip: "172.20.20.110"
         master.vm.hostname = "master"
         #Only configure port to host for master
         master.vm.network :forwarded_port, guest: 8081, host: 8081
    end

    #Setup Slave Nodes
    (1..NODE_COUNT).each do |i|
		config.vm.define "slave-0#{i}" do |slave|
			slave.vm.network :private_network, ip: "172.20.20.11#{i}"
			slave.vm.hostname = "slave-0#{i}"
		end
    end

    if Vagrant.has_plugin?("vagrant-cachier")
	config.cache.scope = :box
    end
end