#!/bin/sh
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color
printf "${BLUE}-----------------------------|                 ${RED}CODE HOP${BLUE}                 |-----------------------------${NC}\n"
printf "${GREEN}Started to delete VMs. Please do not interrupt in between!${NC}\n\n"
export PATH=$PATH:"/c/Program Files/Oracle/VirtualBox"
MASTER_VM_ID=$(VBoxManage list vms | grep "master" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
SLAVE_01_VM_ID=$(VBoxManage list vms | grep "slave-01" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
SLAVE_02_VM_ID=$(VBoxManage list vms | grep "slave-02" | awk '{ print $2 }' | sed "s/{//g"  | sed "s/}//g")
NODE_COUNT=1
if [ ! -z "$SLAVE_02_VM_ID" -a "$SLAVE_02_VM_ID" != " " ]; then
	NODE_COUNT=2
fi

sed -i "s/NODE_COUNT/$NODE_COUNT/g" Vagrantfile

if [ ! -z "$MASTER_VM_ID" -a "$MASTER_VM_ID" != " " ]; then
		MASTER_VM_ON=$(VBoxManage showvminfo $MASTER_VM_ID | grep -c "running (since")
		if [[ "$MASTER_VM_ON" == 1 ]]; then
			printf  "${GREEN}Switching off ${RED}Master VM${NC}\n"
			VBoxManage controlvm $MASTER_VM_ID poweroff
		fi
		printf  "${GREEN}Deleting ${RED}Master VM${NC}\n"
		VBoxManage unregistervm $MASTER_VM_ID --delete
		vagrant destroy master
fi

if [ ! -z "$SLAVE_01_VM_ID" -a "$SLAVE_01_VM_ID" != " " ]; then
		SLAVE_01_VM_ON=$(VBoxManage showvminfo $SLAVE_01_VM_ID | grep -c "running (since")
		if [[ "$SLAVE_01_VM_ON" == 1 ]]; then
				printf  "${GREEN}Switching off ${RED}Slave-01 VM${NC}\n"
				VBoxManage controlvm $SLAVE_01_VM_ID poweroff
		fi
        printf  "${GREEN}Deleting ${RED}Slave-01 VM${NC}\n"
		VBoxManage unregistervm $SLAVE_01_VM_ID --delete
		vagrant destroy slave-01
fi

if [ ! -z "$SLAVE_02_VM_ID" -a "$SLAVE_02_VM_ID" != " " ]; then
		SLAVE_02_VM_ON=$(VBoxManage showvminfo $SLAVE_02_VM_ID | grep -c "running (since")
		if [[ "$SLAVE_02_VM_ON" == 1 ]]; then
				printf  "${GREEN}Switching off ${RED}Slave-02 VM${NC}\n"
				VBoxManage controlvm $SLAVE_02_VM_ID poweroff
		fi
        printf  "${GREEN}Deleting ${RED}Slave-02 VM${NC}\n"
		VBoxManage unregistervm $SLAVE_02_VM_ID --delete
		vagrant destroy slave-02
fi

vagrant global-status --prune
printf "${GREEN}Removing ${RED}vagrant directory ${NC}\n"
rm -rf .vagrant
printf "${GREEN}Removing ${RED}log files ${NC}\n"
rm -rf *.log
printf "${GREEN}Removing ${RED}private keys ${NC}\n"
rm -rf *.ppk
sed -i "s/(1..$NODE_COUNT)/(1..NODE_COUNT)/g" Vagrantfile